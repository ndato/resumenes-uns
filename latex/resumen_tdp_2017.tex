\documentclass[a4paper,11pt,twoside]{article}
\usepackage[latin1]{inputenc}
\usepackage[spanish]{babel}
\usepackage[colorlinks=true]{hyperref}

\title{Resumen de Tecnolog\'ia de Programaci\'on}
\author{Universidad Nacional del Sur}
\date{Segundo cuatrimestre de 2017}

\begin{document}
\maketitle

\abstract{Contenidos te\'oricos de la materia \emph{Tecnolog\'ia de Programaci\'on} por el profesor \emph{Diego C. Matrinez}\footnote{\url{https://cs.uns.edu.ar/~dcm/tdp/}} del \emph{Departamento de Ciencias e Ingenier\'ia de la Computaci\'on}\footnote{\url{https://cs.uns.edu.ar}} de la \emph{Universidad Nacional del Sur}\footnote{\url{https://www.uns.edu.ar}} durante el segundo cuatrimestre del a\~no 2017}

\tableofcontents

\section{SOLID}

\subsection{Open-Closed Principle}
\label{sec:OCD}
Todos los sistemas cambian durante su ciclo de vida. Cuando un cambio simple resulta en una cascada de cambios ese programa tiene un \emph{mal dise\~no}.
Bajo el principio \emph{Abierto-Cerrado}, ante cambios de requerimientos se agrega nuevo c\'odigo pero no se modifica el ya existente.

\begin{description}
	\item[Abierto:] se refiere a la posibilidad de extender la funcionalidad.
	\item[Cerrado:] se refiere a que el c\'odigo no debe ser modificado.
\end{description}

La clave est\'a en clases abstractas. De esta manera manera las clases que implementan la abstraci\'on son cerradas a modificaciones dado que dependen de una clase abstracta ya definida, pero se puede extender la funcionalidad al crear una nueva clase derivada de la abstracta.

Ejemplos de abstracci\'on:
\begin{itemize}
	\item Si se tiene una clase \emph{Cliente} y otra \emph{Servidor}, deber\'ia de haber una clase \emph{ServidorAbstracto} que es utilizada por \emph{Cliente} y la clase \emph{Servidor} derivar de esa.
	\item En vez de tener una clase \emph{Circulo} y otra \emph{Cuadrado} con cada una su m\'etodo \emph{DibujarCirculo()} y \emph{DibujarCuadrado()}, mejor tener una clase abstracta \emph{Figura} con un m\'etodo \emph{Dibujar()} y extender de ah\'i una clase \emph{Circulo} y \emph{Cuadrado}.
\end{itemize}

No se puede tener una clase 100\% cerrada, ya pueden surgir cambios en los requerimientos que inevitablemente van a necesitar modificaciones. El dise\~nador de la clase deber\'a decidir, estrat\'egicamente, bajo que cambios se deber\'a mantener cerrado el dise\~no.
Si se quiere tener una funcionalidad cerrada, se puede agregar un nuevo m\'etodo a la clase abstracta para esa funcionalidad.

El principio \emph{Abierto-Cerrado} es la base de muchas convenciones de la programaci\'on orientada a objetos
\begin{description}
	\item[Encapsulaci\'on:] Haciendo privados a todos los miembros de una clase, se evita que un cambio en los miembros de una clase afecte a todas las funciones la usan.
	\item[Sin variables globales:] Ning\'un m\'odulo que depende de una variable global puede estar cerrado contra cualquier otro m\'odulo que modifica esa variable.
	\item[Identificar tipos en ejecuci\'on es peligroso:] No se pueden derivar nuevas clases para extender la funcionalidad sin modificar
\end{description}

\subsection{Liskov Substitution Principle}
Este principio indica que los m\'etodos que usen referencias a una clase base deben poder usar objetos de clases derivadas sin saberlo. La importancia de esto es que en caso de no aplicarse el principio el m\'etodo que utilize una clase base \emph{debe conocer todas las clases derivadas}, esto viola el Open-Closed Principle (secci\'on~\ref{sec:OCD}). Un ejemplo simple de la violaci\'on al \emph{principio de sustituci\'on de Liskov} es un m\'etodo que recibe un objeto de clase \emph{Forma} y pregunta que forma es para decidir c\'omo dibujarla, esto viola claramente al principio porque ese m\'etodo necesita saber de todas las clases derivadas de \emph{Forma} para poder dibujarla.

Otro caso menos trivial es si se tiene una clase \emph{Rect\'angulo} con 3 m\'etodos: asignarle un alto, un ancho, y calcular el \'area. De esa clase se deriva otra llamada \emph{Cuadrado}, lo cual suena l\'ogico de hacer, y como el cuadrado tiene alto igual al ancho, se sobrecargan esos m\'etodos para que al indicar el ancho se aplique el mismo al alto y lo mismo al indicar el ancho. El problema surge cuando se usa un objeto de clase \emph{Cuadrado} pero sin saberlo, trabajando sobre la clase \emph{Rect\'angulo}, la violaci\'on se da porque el \emph{Cuadrado} est\'a cambiando el comportamiento del \emph{Rect\'angulo} y uno esperar\'ia que si aplica el alto 10 y luego ancho 5, el resultado del \'area sea 50 pero dado que se estaba trabajando con un \emph{Cuadrado} sin saberlo el \'area va a ser 25.

El problema entre \emph{Cuadrado} y \emph{Rect\'angulo} es que, si bien parecer\'ia ser correcto derivar una de la otra, resulta ser que no todos los rect\'angulos son cuadrados y el cuadrado no tiene exactamente los mismos comportamientos que un rect\'angulo. Para cumplir con el principio de sustituci\'on de Liskov todas las clases derivadas deben mantener el comportamiento que el cliente espera de la case base.

Este principio est\'a tambi\'en relacionado con el dise\~no por contrato, dado que la clase base deber\'ia tener precondiciones sobre sus m\'etodos y en las clases derivadas dichas precondiciones no pueden ser ni m\'as fuertes ni contradecir a las precondiciones de la clase base.

\subsection{Dependency Inversion Principle}
Se podr\'ia considerar que un software tiene un mal dise\~no cuando cumple algunas o todas de las siguientes caracter\'isticas:
\begin{description}
	\item[Rigidez.] Es dificil realizar cambios porque cada cambio afecta muchas otras partes del sistema.
	\item[Fragilidad.] Cuando se realiza un cambio, partes inesperadas del sistema se rompen.
	\item[Inmibilidad.] Es dificil reutilizar en otra aplicaci\'on porque no puede ser desenredada de la aplicaci\'on actual.
\end{description}

Lo que hace que un dise\~no sea rigido, fr\'agil e inamovible es la interdepencendia entre los m\'odulos. Cuando hay mucha interdependencia, un cambio de un m\'odulo va a requerer cambiar muchos otros m\'odulos dependendientes en un efecto cascada, y cuando esa cascada de cambios no puede ser predecida el impacto de los cambios no puede ser estimado y pueden terminar en errores en partes inesperadas del sistema.
Y a su vez, al haber tanta interdependencia se hace dificil separar un m\'odulo para ser reutilizado.

Para ejemplificar el problema, se puede imaginar un programa con 3 m\'odulos: \emph{Teclado}, \emph{Impresora} y \emph{Copiar}. El m\'odulo \emph{Copiar} lee caracteres del \emph{Teclado} y los escribe en la \emph{Impresora}. Si bien hay separaci\'on en diferentes m\'odulos, \emph{Copiar} no puede ser utilizado en ning\'un otro contexto que no sea leer de un teclado e imprimirlo. Se puede pensar en mejorar el programa y agregar diferentes opciones de salida, d\'andole la opci\'on al usuario de elegir entre escribir en una impresora o un diskette, pero el problema es que ahora \emph{Copiar} tiene que tener varios \emph{if} para decidir si se escribe en una impresora o en un diskette.

Se puede notar que en el problema anterior el m\'odulo de m\'as alto nivel, \emph{Copiar} --que tiene la l\'ogica abstracta de leer de un lado y escribir en otro-- , depende de m\'odulos de m\'as bajo nivel. Si se lo pudiera hacer independiente se lo podr\'ia reutilizar para cualquier programa que requiera copiar de un lado y pegar en el otro.

Una forma de solucionarlo es hacer una clase abstracta \emph{Lector} y otra \emph{Escritor} y que \emph{Copiar} utilize dichas clases para leer de \emph{Lector} y escribir en \emph{Escritor} sin importar la implementaci\'on espec\'ifica de m\'as bajo nivel. Luego se pueden hacer m\'odulos que hereden de las clases abstractas para soportar leer y escribir de diferentes dispositivos y el m\'odulo \emph{Copiar} se puede reutilizar independientemente del teclado o la impresora. \emph{Copiar} depende de clases abstractas y las implementaciones dependen de la misma abstraci\'on, las dependencias han sido \emph{invertidas}.

El \emph{principio de la inversi\'on de dependencia} entonces se refiere a:
\begin{itemize}
	\item M\'odulos de alto nivel no deben depender de m\'odulos de bajo nivel. Ambos deben depender de abstracciones.
	\item Las abstracciones no deben depender de los detalles. Los detalles deben depender de las abstracciones.
\end{itemize}
Una arquitectura bien estructurada tiene capas claramente definidas, donde cada una provee un conjunto coherente de servicios a travez de una itnerfaz bien definida. Una interpretaci\'on simple llevar\'ia a pensar que cada capa utiliza directamente a las capas inferiores, dependiendo de ellas. Un modelo m\'as apropiado es utilizar clases abstractas para que cada capa utilize y dependa de esas abstracciones, rompiendo asi las dependencias directas entre las capas.

\subsection{Interface Segregation Principle}
Este principio trata sobre las desventajas de interfaces \emph{gordas} o \emph{contaminadas}, estas son las que se pueden descomponer en grupos de funciones, con cada grupo sirviendo a diferentes clientes. Asi algunos clientes usan un grupo y otros clientes usan otro grupo.

En un ejemplo donde se tiene una clase \emph{Puerta} que puede estar abierta o cerrada, se decide luego que ser\'ia \'util que se incorpore una alarma si la puerta queda abierta luego de cierto tiempo. Se propone hacer una clase \emph{Alarma} que dado un tiempo invoca a un m\'etodo \emph{alarma} de una clase abstracta \emph{ClienteAlarma}. Para incorporar esto a la puerta se termina agregando una clase \emph{PuertaConAlarma} que hereda de \emph{Puerta} la cual a su vez hereda de \emph{ClienteAlarma}.

De esa forma la clase \emph{Puerta} queda \emph{contaminada} con el \emph{ClienteAlarma}, puede haber clientes de \emph{Puerta} que no quieran la funcionalidad de una alarma y si \emph{ClienteAlarma} puede requerir cambios en \emph{Puerta} aun en los sistemas que no usen la alarma.

La alternativa para satisfacer el principio de separaci\'on de interfaces, es hacer una clase \emph{Puerta} abstracta y una clase \emph{ClienteAlarma} abstracta y que \emph{PuertaConAlarma} implemente \emph{Puerta} pero que use un \emph{AdaptadorPuertaAlarma}, el cual implementa la clase abstracta \emph{ClienteAlarma}.

Otra alternativa es usar herencia m\'ultiple y que \emph{PuertaConAlarma} herede tanto de \emph{Puerta} como de \emph{ClienteAlarma}, pero no siempre el lenguaje lo permite.

Otro ejemplo es un cajero autom\'atico que tiene varios m\'odulos, uno para depositar, otro para transferir, otro para retirar, etc. A su vez tiene un m\'odulo \emph{Interfaz}. Si esa interfaz contiene tanto lo relacionado a depositar, como a transferir, retirar, etc. entonces va a quedar contaminada, dado que al realizar un retiro hay que usar una parte de la interfaz que es diferente a depositar o a transferir.

Una mejor forma de realizarlo es que haya varias clases abstractas como \emph{InterfazDeposito}, \emph{InterfazRetiro}, etc. y que \emph{Interfaz} herede de todas ellas. Luego, el m\'odulo de retiro recibe un \emph{InterfazRetiro} y cuando se lo quiera invocar se le pasa la \emph{Interfaz} del cajero (que hereda de \emph{InterfazRetiro}). De esta forma cada transacci\'on s\'olo debe de saber sobre sus interfaces y no de todas.

En el caso de que una transacci\'on necesite varias interfaces, est\'a la tentaci\'on de recibir directamente una \emph{Interfaz} asi puede hacer uso de todo. Sin embargo, es preferible que reciba por separado cada una de las interfaces, sin importar que en el fondo se lo invoque con todos los par\'ametros iguales, todos con la misma \emph{Interfaz}.

\subsection{The Single Responsability Principle}
El principio de responsabilidad \'unica se refiere a que nunca debe de haber m\'as de una raz\'on para modificar una clase. Cada responsabilidad que tiene una clase es una posible modificac\'on cuando haya cambios en los requerimientos. Si una clase tiene m\'as de una responsabilidad, entonces habr\'a m\'as de una raz\'on para modificarla.

Cuando una clase tiene varias responsabilidades, estas quedan asociadas, y el cambio en una responsabilidad puede impactar o impedir las otras responsabilidades. Un ejemplo ser\'ia una clase que modela un rect\'angulo y que a su vez lo dibuja en pantalla, violando el principio, entonces ser\'ia conveniente tener separadas las responsabilidades y tener una clase para modelar la geometr\'ia del rect\'angulo y otra para dibujarlo.

En el contexto del principio de responsabilidad \'unica, se define \emph{responsabilidad} como ``una raz\'on de cambio'', si se puede pensar en m\'as de una raz\'on para cambiar la clase entonces hay m\'as de una responsabilidad.

Otro ejemplo es una clase \emph{Modem} que tenga m\'etodos como \emph{Marcar}, \emph{Transmitir}, \emph{Recivir} y \emph{Colgar}. Si bien parece razonable, hay 2 funcionalidades distintas: la conecci\'on y la comunicaci\'on. Se podr\'ia separar entonces en 2 interfaces diferentes, una engargada de la conecci\'on y otra de la comunicaci\'on. Si bien no es necesario y aveces no es aconsejable, se puede hacer una nueva clase \emph{Implementaci\'onModem} que implemente las 2 interfaces.

\end{document}

