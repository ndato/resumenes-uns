PDFLATEX=pdflatex
RM=rm
LATEXDIR=latex

TEXFILES := $(wildcard $(LATEXDIR)/*.tex)
OUTPUT := $(notdir $(patsubst %.tex,%.pdf,$(TEXFILES)))

.PHONY: all clean distclean

all: $(OUTPUT)

distclean:
	- $(RM) *~ *.aux *.log *.toc *.out

clean:
	- $(RM) *~ *.aux *.log *.toc *.out *.pdf

%.pdf: $(LATEXDIR)/%.tex
	$(PDFLATEX) $<
	$(PDFLATEX) $<

